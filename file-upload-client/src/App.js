import { Form, Button, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';
import cors from 'cors';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';


import React, { Component } from 'react'

export default class App extends Component {
  constructor(){
    super();

    this.state = {
      file: '',
      filename: '',
      isUploaded: false
    }
  }

  handleFileUpload = (e) => {
    let file = e.target.files[0];
    this.setState({
      file: file,
      filename: file.name
    });
  }






    // e.preventDefault();

    // let reader = new FileReader();
    // let file = e.target.files;

    // reader.onloadend = () => {
    //   this.setState({
    //     file: file,
    //     isUploaded: true
    //   });
    // }

    // let fileArray = [].slice.call(file);

    // fileArray.forEach(element => {
    //   this.setState({
    //     filename: element.name
    //   });
    // });
  

  handleSubmission = (e) => {
    e.preventDefault();
   
    axios.get(`http://localhost:4000/getUrl/${this.state.filename}`).then(data => {
      //printing the url for the file comming from minio....
      console.log(data.data);
      console.log(this.state.filename);
      let url = data.data;
      let head = {
        headers: {'Access-Control-Allow-Origin': true}
      }


      //putting post request to that url with the file in that request.....
      axios.put(`${url}`, this.state.file).then(data => {
        console.log("successfully uploaded the file.....");
      }).catch(err => {
        console.log("SOMETHING WENT WRONG WHILE UPLOADING THE FILE INTO URL ERROR SAYS: "+err);
      })



    }).catch(err => {
      console.log("SOMETHING WENT WRONG WHILE GETTING THE URL FOR THE FILE, ERROR SAYS: "+err);
    })
  }
  render() {
    return (
    <div className="App">
       <Form onSubmit={this.handleSubmission}>
         <FormGroup>
           <Label for="exampleFile">File</Label><br /><br />
           <Input type="file" name="file" id="exampleFile" onChange={this.handleFileUpload}/><br />
           <FormText color="muted"><br />
          </FormText>
        </FormGroup><br />
        <Button>Submit</Button>
      </Form>
    </div>
    )
  }
}





//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// function App() {
//   const [selectedFile, setSelectedFile] = useState();
// 	const [isSelected, setIsSelected ]  = useState(false);

//   const changeHandler = (event) => {
// 		setSelectedFile(event.target.files[0]);
// 		setIsSelected(true);
// 	};




//   const handleSubmission = () => {
// 		const formData = new FormData();

// 		formData.append('File', selectedFile);
//     //console.log(formData.values);

//     axios.get(`http://localhost:5000/getUrl/name=${selectedFile.name}`).then(data => {
//       console.log("REQUEST IS SUCCESSFULL AND THE DATA IS : "+data);
//     }).catch(err => {
//       console.log("SOME ERROR GENERATED......");
//     })
// 	};
	



//   return (
//     <div className="App">
//       <Form onSubmit={handleSubmission}>
//         <FormGroup>
//           <Label for="exampleFile">File</Label><br /><br />
//           <Input type="file" name="file" id="exampleFile" onChange={changeHandler}/><br />
//           <FormText color="muted"><br />
//             {isSelected ? (
//                   <div>
//                     <p>Filename: {selectedFile.name}</p>
//                     <p>Filetype: {selectedFile.type}</p>
//                     <p>Size in bytes: {selectedFile.size}</p>
//                     <p>
//                       lastModifiedDate:{' '}
//                       {selectedFile.lastModifiedDate.toLocaleDateString()}
//                     </p>
//                   </div>
//             ) : (
//                   <p>Select a file to show details</p>
//             )}
//           </FormText>
//         </FormGroup><br />
//         <Button>Submit</Button>
//       </Form>
//     </div>
//   );
// }

// export default App;
